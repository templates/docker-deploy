FROM python:3.10-slim

RUN apt-get update && apt-get install --no-install-recommends -y bash nano mc

RUN mkdir /src
WORKDIR /src
COPY . /src/
RUN pip install -r requirements.txt
ENTRYPOINT ["python","main.py"]