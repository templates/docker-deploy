# Template: docker build and deploy

This a gitlab CI/CD template for building a docker image and deploying a docker stack to a certain machine via environment variables within gitlab.

## Getting started
### Within your project
In your own project make sure the following files are present in the root of your project

1. Dockerfile 
   1. Docker file that has all the necessary information to run your code
   2. **Example Dockerfile** for a standard python project with a ```main.py``` and some libraries in a ```requirements.txt``` added as a template here
2.  docker-compose.yaml
    1. docker-compose file with at least your own service, a network for internal docker container communication and the location of the environment file 
    2. **Example docker-compose** for a single service added as a template here
3.  gitlab-ci.yml
    1. the gitlab-ci.yml template that is in this project
    2. :exclamation: change the name within this template to your own service name and remove template from the name (template.gitlab-ci.yml -> .gitlab-ci.yml))
    
###  On gitlab
To make sure variables and the build+deploy job are not dependent, add the following variables to gitlab
1. Create a new gitlab environment here: https://git.astron.nl/grafana/docker-deploy/-/environments/new
2. Go to the CI/CD variables section (for example: https://git.astron.nl/templates/docker-deploy/-/settings/ci_cd -> variables)
3. Expand the section and add the following variables **as a variable** and don't forget to set your environment!:
   1. DEPLOY_HOST
      1. Where the docker service(s) should run.
   2. SERVICE_DIR 
      1. Where the static files are stored like the docker-compose.yaml.
   3. DEPLOY_USER: The user that has read, write, and execute permissions for your `SERVICE_DIR`
      1. The private key `SSH_PRIVATE_KEY` for an ssh connection to your `DEPLOY_HOST`. 
      2. The known hosts file `KNOWN_HOSTS` which ensures it deploys to the correct machines.
        
       _Note: it's best to inherit the private key and known hosts file from the group settings or change this setup with [deploy tokens](https://git.astron.nl/help/user/project/deploy_tokens/index.md)_
4. Add the environment variables that are used by your docker service **as a file** called `DOT_ENV` and don't forget to set your environment!:

![](gitlab-vars.png)

